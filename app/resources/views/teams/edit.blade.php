@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <form method="POST">
          {{ csrf_field() }}
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" required value="{{ $team->name }}">
            </div>
            <div class="form-group">
              <label for="color">Color</label>
              <input type="text" class="form-control" id="color" name="color" required value="{{ $team->color }}">
            </div>
            <div class="form-group">
              <label for="token">Token</label>
              <input type="text" class="form-control" id="token" name="token" required value="{{ $team->token }}">
            </div>
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

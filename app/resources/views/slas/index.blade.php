@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <a href="{{ route('slas/edit', 'new') }}" class="btn btn-primary btn-block">Create New SLA</a>
      <table class="table table-bordered">
        <tr>
          <th>ON/OFF</th>
          <th>ID</th>
          <th>Name</th>
          <th>Point</th>
          <th>Description</th>
          <th></th>
        </tr>
        @foreach($slas as $sla)
        <tr>
          <td>
            @if($sla->is_enabled == 1)
              <a href="#" class="btn btn-primary" onclick="event.preventDefault();document.getElementById('toggle-{{ $sla->id }}').submit();">ON</a>
            @else
              <a href="#" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('toggle-{{ $sla->id }}').submit();">OFF</a>
            @endif
            <form id="toggle-{{$sla->id}}" action="{{ route('slas/toggle') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
              <input type="hidden" name="id" value="{{ $sla->id }}">
            </form>
          </td>
          <td>{{ $sla->id }}</td>
          <td>{{ $sla->name }}</td>
          <td>{{ $sla->point }}</td>
          <td>{{ $sla->body }}</td>
          <td>
            <a href="{{ route('slas/edit', $sla->id) }}" class="btn btn-info">Edit</a>
                &nbsp;
                <a href="#" class="btn btn-danger" onclick="if(confirm('Are you sure you want to delete?')){event.preventDefault();document.getElementById('delete-{{ $sla->id }}').submit();}">Del</a>
                <form id="delete-{{ $sla->id }}" action="{{ route('slas/delete', $sla->id) }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                  <input type="hidden" name="id" value="{{ $sla->id }}">
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    </div>
  </div>
</div>
@endsection

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $teams = Team::orderBy('id', 'desc')->get();
        return view('teams/index', ['teams' => $teams]);
    }

    public function edit($id)
    {
        $team = Team::find($id);
        if(is_null($team))
        {
            $team = new Team;
            $team->generateToken();
        }
        
        return view('teams/edit', ['team' => $team]);
    }

    public function post_edit($id, Request $request)
    {
        $this->validate($request, Team::$validateRule);
        $data = $request->all();

        $team = Team::find($id);
        if(!is_null($team))
        {
            $team->update($data);
        }
        else
        {
            Team::create($data);
        }
        
        return redirect('teams');
    }

    public function delete(Request $request)
    {
        $team = Team::find($request->id);
        if(!is_null($team))
        {
            $team->delete();
        }

        return redirect('/teams');
    }
}

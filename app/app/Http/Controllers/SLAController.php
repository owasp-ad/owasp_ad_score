<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sla;

class SLAController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $slas = Sla::orderBy('id', 'desc')->get();
        return view('slas/index', ['slas' => $slas]);
    }

    public function edit($id)
    {
        $sla = Sla::find($id);
        if(is_null($sla))
        {
            $sla = new Sla;
        }
        
        return view('slas/edit', ['sla' => $sla]);
    }

    public function post_edit($id, Request $request)
    {
        $this->validate($request, Sla::$validateRule);
        $data = $request->all();

        $sla = Sla::find($id);
        if(is_null($data['body']))
        {
            $data['body'] = '';
        }
        if(!is_null($sla))
        {
            $sla->update($data);
        }
        else
        {
            Sla::create($data);
        }
        
        return redirect('/slas');
    }

    public function delete(Request $request)
    {
        $sla = Sla::find($request->id);
        if(!is_null($sla))
        {
            $sla->delete();
        }

        return redirect('/slas');
    }

    public function toggle(Request $request)
    {
        $sla = Sla::find($request->id);
        if(!is_null($sla))
        {
            if($sla->is_enabled == 1)
            {
                $sla->is_enabled = 0;
            }
            else
            {
                $sla->is_enabled = 1;
            }
            $sla->save();
        }

        return redirect('/slas');
    }
}

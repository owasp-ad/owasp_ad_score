<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slalog extends Model
{
    protected $guarded = ['id', 'created_at'];

    public function team()
    {
        return $this->belongsTo('App\Team');
    }

    public function sla()
    {
        return $this->belongsTo('App\Sla');
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class UserAdd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'useradd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add user script.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask("name:");
        $email = $this->ask("email:");
        $password = $this->secret("password:");
        $user = User::Add($name, $email, $password);
        $this->info("user {$user->name} was created");
    }
}
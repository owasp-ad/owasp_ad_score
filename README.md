# OWASP A&D Score Server

## Prerequisite
* make
* composer
* yarn
* nodejs
* npm
* php-mbstring
* php-dom
* docker
* docker-compose
* libpng-dev

## How to set up on Ubuntu
```
$ sudo apt update
$ sudo apt upgrade
$ sudo apt install make composer nodejs npm php-mbstring php-dom docker docker-compose libpng-dev
$ sudo npm install -g yarn
$ git clone https://gitlab.com/owasp-ad/owasp_ad_score.git
$ cd owasp_ad_score
$ composer update -d app
$ cp -n _env .env
$ make setup
$ sudo make up
$ sudo make migrate
$ chmod 777 app/storage/logs/
$ chmod 777 app/storage/framework/views/
$ chmod 777 app/storage/framework/cache/
$ chmod 777 app/storage/framework/sessions/
$ chmod 777 app/storage/framework/testing/
```

#### Creating admin user for the score server on docker container
```
$ sudo docker exec -it owaspad_php-fpm_1 sh
/var/www/app # php artisan useradd
 name::
 > test

 email::
 > test@example.com

 password::
 >

user test was created
```

## How to set up on CentOS

```
$ cp _env .env
$ make setup && make up
```

#### Creating admin user for the score server on docker container

``` 
/var/www/app # php artisan useradd

 name::
 > test

 email::
 > test@example.com

 password::
 >

user test was created
```

## SLA api
```
curl -X POST -H 'Content-Type: application/json' -i http://localhost:8888/api/sla/check --data '{"token": "8c282fe35995ba701b843a21b86dca43189c24c9", "team_id": 2, "sla_id": 2, "is_success": 0}'
```

## Cron
```
* * * * * docker exec [owaspad_php-fpm_1] /usr/local/bin/php /var/www/app/artisan schedule:run
```

## Quick Start Guide
1. seting up laravel environment.
1. setting cron for SLA.
```
* * * * * /usr/local/bin/php /var/www/app/artisan schedule:run
```
1. modifiy .env file like below.
```
SLA_PENALTY=.03               # percentile of penalty
SLA_ITERATE=3                 # SLA iteration time (min)
SLA_START=2018-06-24 10:00:00 # SLA Start datetime
SLA_END=2018-06-25 10:00:00   # SLA End datetime
```
3. create user accont using below command.
``` 
php artisan useradd
```
4. login web page.
1. create teams in /teams/.
1. create SLAs in /slas/.
1. this score server's SLA using api. 
Mainly, we using python script for the api.  
api detail is below.
* token is setting in /teams/.
* team_id is in /teams/.
* sla_id is in /slas/.
* is_success is 0(false) / 1(success).
```
curl -X POST -H 'Content-Type: application/json' -i http://localhost:8888/api/sla/check --data '{"token": "8c282fe35995ba701b843a21b86dca43189c24c9", "team_id": 2, "sla_id": 2, "is_success": 0}'
```  
8. Then SLA calculate start from SLA_START automatically.

